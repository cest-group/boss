FROM python:3.11-slim-bookworm

RUN apt-get update && apt-get install \
    git \
    -yq --no-install-suggests --no-install-recommends --allow-downgrades --allow-remove-essential --allow-change-held-packages \
  && apt-get clean

RUN pip3 install --upgrade --no-cache-dir \
  pip \
  setuptools

RUN pip3 install --upgrade --no-cache-dir \
  twine \
  "GPy>=1.13.1" \
  "scipy>=1.8" \
  "numpy>=1.20" \
  "matplotlib>=3.5,<3.9" \
  pytest \
  coverage \
  sphinx-rtd-theme \
  numpydoc \
  sphinx-copybutton \
  sphinx-autoapi

CMD /bin/bash
