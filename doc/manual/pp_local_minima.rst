.. _pp_local_minima:

Local minima detection
======================

.. figure:: ../figures/pp_local_minima.png
   :width: 100.0%

   Fig. 1: In postprocessing, all local minima (red crosses) of the GP model
   are detected automatically using minimizers.

The GP model represents an N-dimensional property landscape defined
across the variable domain :math:`\mathcal{X}`, and it is possible to
traverse it for information. While it can never be fully known (high
information content), we can compute its value at any state :math:`x_i \in
\mathcal{X}`. This allows use of standard minimizer routines (bfgs, etc) to
determine local minima.  These functions typically follow local gradients to
the nearest minimum so the initial point on the landscape matters. We conduct
many minimization searches starting from a fraction of the lowest energy data
acquisitions, set with the keyword :ref:`pp_local_minima
<sec_pp_local_minima>`.  Acquisitions span the :math:`\mathcal{X}` domain space
relatively well and this approach allows us to identify many distinct minima in
N dimensions, although many duplicate minima are found as well. While nominal
cleaning of duplicate minima is performed, it is up to the user to carefully
review the output minima list.

