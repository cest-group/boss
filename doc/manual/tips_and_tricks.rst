Recommended workflow
++++++++++++++++++++

.. raw:: html

   <h3>Before starting</h3>

* Consider your research question. What is the target objective? What
  are the relevant parameters?

* Define search variables and their domains. Are all variables equally
  important? Are there any regions that must be excluded from the
  search space (see :ref:`cost functions <Cost functions>`)?
  
* Use periodic kernels when possible. This will speed up model
  convergence with less iterations.

* Consider the computational implementation. Which tasks should the
  user function perform? Is CLI or API interface more suitable for
  your case?
  
* Apply :ref:`parallel optimization <sec_parallel_optims>` to speed up
  BO iterations on multi-core CPUs.

.. raw:: html

   <h3>Initial tests</h3>
   
* Starting with low-dimensional (1D--2D) searches is highly
  recommended. This will provide information on the value range and
  complexity of the target function (for example the number of local
  minima) and help to determine suitable search ranges.
  
* Compute the true function (:ref:`pp_truef_npts <sec_pp_truef_npts>`)
  and check if the model converges to the correct function. **Note:**
  This is recommended only for 1D--2D and/or when data acquisitions
  are inexpensive.

* With the converged low-dimensional models, take note of final GP
  hyperparameters.  You can start higher-dimensional runs from these
  values to accelerate convergence.

* If you encounter convergence issues, consider these actions:

    * Most common reason behind convergence issues are wrong data and
      errors in the user function, so it is recommended to first
      examine postprocessing plots and model images.
      
    * Next consideration is whether the surrogate models are
      qualitatively converged but numerical data is not. In that case,
      the convergence criterion may be set too low.
      
    * In the rare case the models are not converging, one could remove
      hyperparameter priors and manually input a range of kernel
      hyperparameters that are more suitable for describing the use
      case (e.g. through grid search).
      
    * Increasing noise could help, and consider if the kernel reflects
      the data well. If the data is not periodic, a periodic kernel
      will fail. 


.. raw:: html

   <h3>Quality checks</h3>

* Evaluate model convergence based on identified minima (global and
  local), GP hyperparameters, and 1D--2D cross-sections of the model.
  Converged model indicates sufficient dataset size for optimal
  learning. In contrast, oscillations in the identified minima and
  hyperparameters suggest model instability and further BO iterations
  are needed.

* Key performance indicators (case specific) are: How many data points
  are necessary to i) locate the global minimum, or ii) converge the
  entire surrogate model (all minima and MEPs between them).

* Inspect how increasing dimensions and the number of minima affects
  model convergence. Do some dimensions have particularly complex
  landscapes that slow down convergence?


.. raw:: html

   <h3>High-dimensional simulations</h3>

To speed up model convergence for high-dimensional searches, we
recommend the following:

* Start with optimal GP hyperparameters and priors (see :ref:`keywords
  <GP Hyperparameters>`).

* Initialize the model with previously acquired data points from
  low-dimensional searches.

* Utilize symmetry wherever possible: i) In case of periodic kernels,
  use the smallest possible periodic boundary to minimize the size of
  search space. ii) Multiply the number of data points according to
  symmetries (see :ref:`tutorial <User function symmetry>`) to gain
  several points from each data acquisition.



.. raw:: html

   <h3>Troubleshooting</h3>

* The most common runtime error with BOSS occurs when the user
  function script fails to compute and/or return the label to an
  acquisition query.  This can be identified by correct BOSS
  initalisation in the boss.out file with the stop occurring at the
  first ``initpt``, even though Python messages can be confusing.
  Please ensure that the external scripts function correctly before
  plugging them into BOSS.

* BOSS iterations carry the overhead of computing local minimum
  estimates which are needed for convergence tracking, but not for BO.
  If iterations take too long, you can switch this off by setting the
  :ref:`minfreq <sec_minfreq>` keyword to 0.

* In the limit of 1000s of data points, BOSS iteration times can be
  long (>30min). BO acquisition functions are most effective with
  smaller datasizes; with large datasets, iterative acquisitions will
  take a long time, and a randomly sampled dataset could be used to
  build surrogate models instead.
