BOSS definitions
++++++++++++++++

**Ensemble definitions**. The state vector :math:`x` encodes the
search variables. At iteration i, data is sampled at :math:`x_i \in
\mathcal{X}`, and observations are defined as :math:`f(x_i)=y_i`. The
ensemble of sampled configurations supplies the dataset :math:`({\bf
x}, {\bf y})`. The domain :math:`\mathcal{X}` is :math:`N`-dimensional
(see :ref:`Degrees of Freedom<sec_dof>`) and known (the boundaries
serve as input).  :math:`y` values are defined across an unknown
domain, which is estimated at the start of the simulation.

**Surrogate/GP model.** True function :math:`f` is modelled by a GP
with prior mean :math:`m` and kernel function
:math:`k_{\theta_i}` at every iteration :math:`i`. Prior mean is
chosen as zero (uninformative but general).

  .. math:: f \sim \mathcal{GP}(0,k_{\theta_i})

The observations are modeled as noisy: :math:`y_i= f(x_i)+\epsilon_i`,
where :math:`\epsilon_i \sim N(0, \sigma_n^2)`, but in practice the
noise level :math:`\sigma^2_n` is chosen to be small. At any point
:math:`x^*`, one can construct the surrogate model by evaluating the
posterior mean :math:`\mu(x^*)` and variance :math:`\upsilon(x^*)`
using the Bayes’ theorem. More accurately, the model at point
:math:`x^*`, conditional on data (:math:`{\bf x}, {\bf y})` and model
hyperparameters :math:`\theta_i \in \Theta` being known, follows a
normal distribution

  .. math::

     \begin{aligned}
     f(x^*) \,|\, {\bf x}, {\bf y}, \theta_i &\sim N(\mu(x^*), \, \upsilon(x^*)), \;\text{where} \\
     \mu(x^*) &= {\bf k}_*^\top (K + \sigma^2_n I)^{-1} {\bf y}, \\
     \upsilon(x^*) &= k_{\theta_i}(x_*, x_*) - {\bf k}_*^\top (K + \sigma^2_n I)^{-1}\, {\bf k}_*.\end{aligned}

Notation has been shortened here by denoting with :math:`K` the
elementwise covariance matrix of the data points :math:`{\bf x}`,
corresponding to :math:`k_{\theta_i}`, and the vector of covariances
between point :math:`x_*` and data points :math:`{\bf x}` with
:math:`{\bf k}_*`.

.. figure:: ../figures/boss_def.png
   :width: 90.0%

   Figure 1: Postprocessing output from a BOSS 1D simulation at iteration
   :math:`i`: a) GP surrogate model printout; b) For printing, the model
   is evaluated on a dense grid as illustrated at point :math:`x^*`; c)
   acquisition function printout.

**Hyperparameters.** At iteration :math:`i`, GP hyperparameters
:math:`\theta_i` comprise the ARD kernel lengthscales
:math:`l^{(1)}, \ldots, l^{(N)}`, kernel variance :math:`\sigma^2`,
fixed periods :math:`p^{(1)}, \ldots p^{(N)}` for the periodic kernel
and the fixed noise level :math:`\sigma^2_n`. ARD (Automatic Relevance
Determination) kernels are non-isotropic, considering each dimension to
be independent from the others. In the case of the mixed kernel,
:math:`\theta_i= \left( \theta_i^{SP},  \theta_i^{RBF} \right)`, where
the RBF (Radial Basis Function, or Squared Exponential) kernel differs
from the SP (standard periodic) case by the lack of fixed period
hyperparameters. The nonfixed GP hyperparameters can be optimized by
maximizing the marginal likelihood, the probability of observing
:math:`{\bf y}`, given locations :math:`{\bf x}` and the hyperparameters
:math:`\theta_i` themselves as known:

.. math:: \theta_{i,new} = \underset{\theta^*_i \in \Theta}{\mathop{\mathrm{arg\,max}}}\ p({\bf y} \,|\, {\bf x}, \theta^*_i).

**Special Data Points.** Best observation (e.g. lowest energy) from all
acquired data points is :math:`(x_{best}, y_{best})`. Note that
:math:`y_{best} \approx \mu(x_{best})` since the small noise level means
that, in practice, :math:`\mu` passes very near to the data points. The
predicted optimal point and its predicted global minimum are:

.. math::

   x_{glmin} = \underset{x^*\in\mathcal{X}}{\mathop{\mathrm{arg\,min}}}\,{\mu(x^*)}, \qquad
   \mu_{glmin} = \mu(x_{glmin})

Convergence may be monitored as :math:`\mu(x_{glmin})-f(x_{glmin})`, which
describes the difference between the surrogate model and true function
at the predicted optimal point. Location of the next acquisition
:math:`x_{next}` is given by the acquisition function as follows:

.. math::

   a(x^*) = a(x^*; {\bf x}, {\bf y}, \theta_i), \qquad
   x_{next} = \underset{x^*\in\mathcal{X}}{\mathop{\mathrm{arg\,max}}}\,{a(x^*)}

The corresponding observation is :math:`y_{next}` (see Fig. 1c.). In the following
iteration, :math:`(x_{next}, y_{next})` is added to the dataset.

**Multi-task Gaussian Processes.** BOSS supports modeling multiple
unknown functions using a multi-task GP model where each task
:math:`t` corresponds to an unknown function :math:`f_t`. These can be
different data channels, objectives, fidelities, or simulators. Since
an observation :math:`y_i = f_{t_i}(x_i) + \epsilon_i` now depends on
both the task :math:`t_i` and state :math:`x_i`, the multi-task model
operates in an extended input domain with input pairs :math:`(t, x)`
and has more hyperparameters than a single-task model. BOSS uses an
intrinsic coregionalization model (ICM) that assumes a linear
correlation between tasks and relies on a separable kernel
construction

.. math::

   k((t, x), (t’, x’)) = k_\phi(t, t’) k_\theta(x, x’),

where :math:`k_\phi` denotes a task covariance kernel and
:math:`k_\theta` is the standard covariance kernel used in
single-task models. The new hyperparameters :math:`\phi` include a
low-rank matrix :math:`W` and a vector :math:`\kappa`. These
define a task covariance matrix and regulate the exchange of
information between different tasks. At BO iteration :math:`i`, the
kernel is evaluated with parameters :math:`\phi=\phi_i` and
:math:`\theta=\theta_i`. 

**Pareto analysis.** A Pareto solution in multi-objective optimization is a set of decision variables that cannot be improved in one objective without compromising another. It is an "n"-dimensional surface in the solution space where no other solution dominates it. Therefore, the Pareto solutions represent trade-offs between different objectives.

Mathematically, consider an input vector :math:`X` and its corresponding objective function (:math:`Y`), such that,

.. math::

   X = (x_1, x_2, ... ),
   Y_i = (f_i(x_1), f_i(x_2), ...)

Then the Pareto front (PF) is expressed as,

.. math::

   PF = \{ x \in S :  \nexists\,  x' \in S, \nonumber \\ \text{such that}\,,  f_i  (x') \geq f_i (x)\, \text{for all } i \},

**Pearson correlation coefficient matrix.** A correlation heatmap matrix visually represents a correlation matrix, where the color of each grid cell indicates the correlation between two objectives. The intensity of the color signifies the correlation's strength.

We can calculate the Pearson correlation coefficient matrix, :math:`R`, using the formula:

.. math::

   R_{ij} = \frac{C_{ij}}{C_{ii}C_{jj}}

Where :math:`C` is the Correlation matrix. Pearson Correlation heatmaps are therfore a valuable tool for data analysis, as they can help identify relationships between objectives and detect multicollinearity.

**Evolutionary multi-objective (EMO) algorithm.** While identifying the Pareto-optimal set is a crucial step in solving a Multi-Objective Optimization (MOO), it is not the final goal.
In real-world applications, a specific solution or a small subset of solutions must be selected.
This chosen solution belongs to the Pareto front and depends on the choise of the decision maker.
In the BOSS code, we use an Evolutionary Multi-Objective Optimization (EMO) technique, weighted reference point - EMO,  to find the Pareto front and then employed the following decision-making criteria to select solutions from it,

.. math::

   d(x) = \Big[\sum_i^n w_i \big(\frac{f_i(x)-z_i^*}{f_i^{max} - f_i^{min}} \big)^p \Big]^\frac{1}{p}

Here, :math:`w` represents the weight, and :math:`z*` is the reference point.
From the set of Pareto solutions, we selected the point with the smallest :math:`d` value as calculated in the above equation. This decision criterion is expressed as :math: `\argmin{d(x)}`.
