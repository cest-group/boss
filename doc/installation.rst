Installation
============
BOSS is written in Python 3 and distributed as a PyPI package. 

Virtual environments (recommended)
++++++++++++++++++++++++++++++++++
We recommend installing BOSS inside a virtual enviroment (``venv``, ``conda``...).

To create a virtual environment *'boss'* with ``venv``:

.. code-block:: sh

    python3 -m venv boss
    source boss/bin/activate


or with ``conda``:

.. code-block:: sh

    conda create -n boss
    conda activate boss


The latest stable BOSS version can then be installed using pip:


.. code-block:: sh

    python3 -m pip install aalto-boss


User installation
+++++++++++++++++++
If you are not using virtual enviroments, we recommend doing a user installation with pip:

.. code-block:: sh

    python3 -m pip install --user aalto-boss

On Linux, this will place the python module under ``$HOME/.local/lib/python3.X/site-packages`` where X should be substituted for the minor version of your Python distribution. During the installation an executable called ``boss``, which is used to run BOSS, is built and placed under ``$HOME/.local/bin``. In order for your BOSS installation to function properly, these paths must (if not already present) be appended to the correct enviroment variables:

.. code-block:: sh

   export PATH=$PATH:$HOME/.local/bin
   # Do not forget to substitute X for your Python minor version 
   export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python3.X/site-packages

To check that the installation was successful, calling the ``boss`` executable without any arguments should result in usage instruction being printed. During the installation you may see a large number of dependencies being installed by pip. Notably, BOSS uses the Gaussian process framework  `GPy <http://github.com/SheffieldML/GPy>`_ as a backend.

Development version
+++++++++++++++++++
To install the development version, clone the source code from `GitLab <https://gitlab.com/cest-group/boss>`_ and install with pip:

.. code-block:: sh

    git clone https://gitlab.com/cest-group/boss.git
    cd boss
    python3 -m pip install --user .

Experimental features
+++++++++++++++++++++
After acquiring the source code, experimental features can be installed from their own development branches:

.. code-block:: sh

    git checkout [branch_name]
    cd boss
    python3 -m pip install --user .

**WARNING:** Experimental features that are in development may produce inaccurate results. Use at your own risk!

Known issues
+++++++++++++++++++
* Rarely, on HPC systems, warnings related to the use of cython in GPy have been observed. This can typically be resolved by using a more fully-featured Python distribution that ships with cython, such as `Anaconda <https://www.anaconda.com/distribution/>`_.
