.. _restarting_py:

Restarting
===============

Note: you can also download this part of the tutorial as a :download:`python script <pyfiles/resuming.py>` or a :download:`notebook <notebooks/resuming.ipynb>`.

If we run BOSS and find that the results are not satisfactory, the optimization
can easily be resumed as long as the original BOMain object has not gone out of scope.

To illustrate this, we consider a 2D function, which has an approximate global minimum :math:`y = 1.463` at :math:`x = (-4.000, -3.551)`.

.. code-block:: python

    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def func_2d(X):
        x = X[0, 0]
        y = X[0, 1]
        z = 0.01 * ((x ** 2 + y - 11) ** 2 + (x + y ** 2 - 7) ** 2 + 20 * (x + y))
        return z

    bo = BOMain(
        func_2d,
        bounds=[[-5.0, 5.0], [-5.0, 5.0]],
        yrange=[0.0, 10.0],
        initpts=5,
        iterpts=10
    )
    res = bo.run()
    print('Predicted global min: {} at x = {}'.format(res.fmin, res.xmin))

Here, we only did 10 BO iterations, which is on the low side for a non-trivial 2D problem. We can improve on our result by resuming our optimization and adding, say 20 additional iterations. To do so, we simply invoke the ``BOMain.run`` method again
and specify the new total number of iterations = 10 + 20 = 30:

.. code-block:: python

    res = bo.run(iterpts=30)
    print('Predicted global min after resuming: {} at x = {}'.format(res.fmin, res.xmin))
    # run postprocessing
    pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
    pp.run()


Restart using boss.rst file
+++++++++++++++++++++++++++

Note: you can also download this part of the tutorial as a :download:`python script <pyfiles/restarting.py>` or a :download:`notebook <notebooks/restarting.ipynb>`.

When BOSS runs, a restart file is produced, by default named ``boss.rst``,
which allows an optimization to be restarted at a later point if the results
were not satisfactory or the original run was somehow aborted. Here,
we demonstrate how the previous 2D optimization (above) can be
restarted using the ``boss.rst`` output file. Before proceeding,
create the ``boss.rst`` file by running the previous example.


.. code-block:: python

    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def func_2d(X):
        x = X[0, 0]
        y = X[0, 1]
        z = 0.01 * ((x ** 2 + y - 11) ** 2 + (x + y ** 2 - 7) ** 2 + 20 * (x + y))
        return z

To recreate a BOMain object we use the ``BOMain.from_file()`` factory method,
when doing so we have the option to change any keywords. Since the run we
are restarting from had 30 iterations, we increase the number to 50 to get
a more accurate minimum prediction.

.. code-block:: python

    bo = BOMain.from_file('boss.rst', f=func_2d, iterpts=50)
    res = bo.run()
    print('Predicted global min after restart: {} at x = {}'.format(res.select('mu_glmin', -1), res.select('x_glmin', -1)))
    # run postprocessing
    pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
    pp.run()

.. note::

    During the restart above we had to redefine the user function and pass it to ``BOMain.from_file()``,
    although it had already been defined during a previous run. If the ``f`` argument to ``from_file()`` is omitted,
    BOSS will try to import the user function used in the previous run. This saves us the trouble of having
    to redefine the function, but can lead to unwanted side effects since any statements within
    the global scope of the previous BOSS Python script will automatically run during the import process.
    This can be avoided by writing BOSS scripts where only function definitions appear in the global scope and 
    any other code is placed under an `if-name-main statement <https://stackoverflow.com/questions/419163/what-does-if-name-main-do>`_.
