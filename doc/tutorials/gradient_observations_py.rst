Gradient Observations
=====================
Note: you can also download this tutorial as a :download:`python script <pyfiles/gradient_observations.py>` or a :download:`notebook <notebooks/gradient_observations.ipynb>`.
We will be minimizing the Forrester function f(x) = (6x - 2) 2 sin(12x -
4) on the interval [0, 1]

.. code:: 

    import numpy as np
    from boss.bo.bo_main import BOMain

The user function must return the function value and gradient in tuple
(y, dy) format:

.. code:: 

    def func(X):
        x = X[0, 0]
        y = (6 * x - 2) ** 2 * np.sin(12 * x - 4)
        dy = 12 * (6 * x - 2) * (np.sin(12 * x - 4) + (6 * x - 2) * np.cos(12 * x - 4))
        return y, dy

.. code:: 

    bounds = np.array([[0.0, 1.0]])

We create the BOMain object with the keyword ``model_name`` set to
``grad``. We can then run the BO as usual (NOTE: including gradient
observations can slow down BOSS, especially at higher dimensions).

.. code:: 

    bo = BOMain(func, bounds, model_name='grad', kernel="rbf", initpts=3, iterpts=7)
    res = bo.run()

Including gradient observations generally lowers the number of
iterations required for a good prediction of the global minimum.

.. code:: 

    print("Predicted global min: ", res.select("mu_glmin", -1))

Note that the observed gradients are concatenated to function values in
the results object:

.. code:: 

    print(res.select("Y"))

Please see the other tutorial notebooks for more detailed descriptions
of results and postprocessing.
