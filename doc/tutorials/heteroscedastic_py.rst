Heteroscedastic noise
=====================
Note: you can also download this tutorial as a :download:`python script <pyfiles/heteroscedastic.py>` or a :download:`notebook <notebooks/heteroscedastic.ipynb>`.

This tutorial demonstrates how to treat heteroscedastic noise problems
with BOSS. Heteroscedasticity implies that the noise can vary across the
search space [1]. Heteroscedastic models are useful in cases where the
objective funtion is known to be heteroscedastic.

.. code:: 

    import numpy as np
    from boss.bo.bo_main import BOMain

In this tutorial, we will minimize the one-dimensional Rastrigin
function :math:`f(x)=10+x^2-10\cos(2\pi x)` in the interval
:math:`-5.12 \le x \le 5.12`. To make the function heteroscedastic, we
add some random noise to it. This is achieved by sampling a value,
``r``, from a normal distribution with a mean of zero and a standard
deviation of ``c``. The value ``r``, multiplied by the true, noiseless
value of the objective function, gives the noise at a given output value
of the objective function. In this example, we set ``c`` to :math:`0.1`.

.. code:: 

    c = 0.1
    
    
    def func(X):
        x = X[0, 0]
        y = 10.0 + x**2 - 10 * np.cos(2 * np.pi * x)  # Ground truth value
        r = np.random.normal(scale=c)
        return y * (1.0 + r)

Next, we present two examples to demonstrate the use of heteroscedastic
models.

Example 1: Simple noise function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A heteroscedastic model requires a noise estimation function, which must
be defined as a Python function. The noise function is used by the model
to estimate noise associated with samples of the objective function. In
this example, the function estimates the noise at :math:`x` to be
:math:`10\,\%` of the predicted function value :math:`f(x)`. Note that
the noise function should output variance values.

The arguments of the noise function are always the following: \*
``hsc_args`` (list) passes additional, user-given arguments to the noise
function. \* ``**kwargs`` (dict) can be used to access the model
data/parameters. It contains the sampled values (``Y``) and locations
(``X``), the GP surrogate (``model``) and the kernel lengthscales
(``lengthscale``).

.. code:: 

    def noise_func1(hsc_args, **kwargs):
        mu_array, _ = kwargs["model"].predict(kwargs["X"])
        noise_array = hsc_args[0] * np.abs(mu_array)
        # Noise array must contain noise variance values,
        # not standard deviation values
        noise_array = [i**2 for i in noise_array]
        return noise_array
    
    
    hsc_args = [0.1]

Next, we create a ``BOMain`` instance and perform the optimization with
``model_name=hsc`` to use a heteroscedastic model. The noise function is
passed to BOSS using the ``hsc_noise`` keyword and the list of
user-given arguments using the ``hsc_args`` keyword.

.. code:: 

    bo = BOMain(
        func,
        bounds=[-5.12, 5.12],
        model_name='hsc',
        yrange=[0., 50.],
        initpts=2,
        iterpts=20,
        hsc_noise=noise_func1,
        hsc_args=hsc_args,
        seed=25,
    )
    
    res = bo.run()
    
    print("Predicted global min: ", res.select("mu_glmin", -1))

For comparison, we can do the same optimization with the standard
homoscedastic model. For this, we need to choose a value for the noise
hyperparameter. Let’s choose it to be the mean noise of the objective
function, which is about 4.49.

.. code:: 

    bo = BOMain(
        func,
        bounds=[-5.12, 5.12],
        yrange=[0.0, 50.0],
        initpts=3,
        iterpts=20,
        noise=4.49,
        seed=25,
    )
    
    res = bo.run()
    
    print("Predicted global min: ", res.select("mu_glmin", -1))

The true value of the global minimum is zero. In this case, the
heteroscedastic model is clearly better. This is usually the case if the
objective function is heteroscedastic. However, if the noise
hyperparameter given to the homoscedastic model corresponds well to the
noise of the objective function around the global minimum, the models
often perform very similarly. The homoscedastic model should be used in
cases where it is sufficient, since the heteroscedastic model is
computationally heavier to run.

Example 2: Known noise of initial data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases, the user might know the noise of the initial data. In
such a case, it is possible to create a noise estimation function that
takes this into account. In this example, we modify the previous noise
function so that the noise values of the initial data are always set to
the same, user-defined values.

.. code:: 

    # Initial data points and noise
    X_init = [-5, 0, 2, 4]
    Y_init = [25, 0, 4, 16]
    noise_init = [6.25, 0.0, 0.16, 2.56]
    
    
    def noise_func2(hsc_args, **kwargs):
        mu_array, _ = kwargs["model"].predict(kwargs["X"])
        noise_array = hsc_args[0] * np.abs(mu_array)
        # Noise array must contain noise variance values,
        # not standard deviation values
        noise_array = [i**2 for i in noise_array]
        # Set the noise of inital data to known values
        noise_array[:4] = np.atleast_2d(noise_init).T
        return noise_array
    
    
    hsc_args = [0.1]

The heteroscedastic model requires noise estimates of the initial data
points for the first optimization of hyperparameters during the model
initialization step. If the noise of the initial data is known, the data
can be passed to the model as a list using the ``noise_init`` keyword.
If initial noise estimates are not specified, the default value of
``10e-12`` is used for all initial data points. After the model
initialization, the noise function is used to estimate noise, even for
the initial data.

.. code:: 

    bo = BOMain(
        func,
        bounds=[-5.12, 5.12],
        model_name='hsc',
        yrange=[0., 50.],
        initpts=4,
        iterpts=20,
        hsc_noise=noise_func2,
        noise_init=noise_init,  # only used during model initialization
        hsc_args=[0.1],
        seed=25,
    )
    
    res = bo.run(X_init=X_init, Y_init=Y_init)
    
    print("Predicted global min: ", res.select("mu_glmin", -1))

[1]
https://en.wikipedia.org/wiki/Homoscedasticity_and_heteroscedasticity
