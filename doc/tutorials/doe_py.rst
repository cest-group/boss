Design of experiments with BOSS
===============================
Note: you can also download this tutorial as a :download:`python script <pyfiles/doe.py>` or a :download:`notebook <notebooks/doe.ipynb>`.
This tutorial demonstrates a simple workflow for performing design of
experiments (DOE) with the help of BOSS. To illustrate the process we
will use data from the paper `Machine Learning Optimization of Lignin
Properties in Green Biorefineries <https://pubs.acs.org/doi/full/10.1021/acssuschemeng.2c01895>`__.
Here, the goal is to maximize the yield of lignin extracted from birch
wood. The yield is a function of two input variables, the P-factor
(quantifying the reaction severity) and the reactor temperature. The
P-factor can range between 500 and 2500 while the temperature ranges
between 180-210 Celsius. To keep things simple we will store and load
data using pandas.

.. code:: 

    # Import necessary modules
    import pandas as pd
    from boss.bo.bo_main import BOMain
    import numpy as np
    from boss.bo.initmanager import InitManager
    
    # Define variable names + bounds
    inputs = ['p_fac', 'temp']
    output = ['yield']
    bounds = np.array([[500., 2500.], [180., 210.]]) 

Step 1: Collect initial data (optional if you already have data)
----------------------------------------------------------------

To initialize the Gaussian process model used in the BO, we must have
access to a small set of data points (for a 2D problem we might use 4 to
12 initial points). If no data has been collected yet, we can generate a
set of suggested input variables using a Sobol sequence, for easy
retrieval and visualization we store the data in a pandas dataframe that
can easily be exported to various formats.

.. code:: 

    # Generate initial data 
    im = InitManager(inittype='sobol', bounds=bounds, initpts=8)
    X_init = im.get_all()
    
    # store data in dataframe
    df = pd.DataFrame({'p_fac': X_init[:, 0], 'temp': X_init[:, 1], 'yield': np.nan})
    
    # inspect data and save to CSV file
    print(df)
    df.to_csv('lignin_data.csv', index=False)

Note how the yield column is empty, we must now measure the yield for
the input variables suggested in the dataframe and update the
corresponding entries. Let’s pretend we did this for our lignin problem
and got the following results:

.. code:: 

    measured_yields = [40.0, 70.3, 91.9, 51.0, 67.8, 84.8, 84.4, 45.3]
    # Normally performing the experiments took some time so we would need to read back our inital data from disk
    df = pd.read_csv('lignin_data.csv', index_col=None)
    df.loc[:, 'yield'] = measured_yields

Step 2: Run one BOSS iteration (this part must be ran many times)
-----------------------------------------------------------------

At this point we have some initial data available and can now perform
one iteration of Bayesian optimization. Note that since experiments must
be physically performed we can only do one BO iteration at a time.
Running the code in this section of the notebook thus amounts to doing a
single iteration of BO, resulting in a single new set of suggested input
variables (or many sets if you use batch acquisitions) for which an
experiment must be performed and the data updated manually.

.. code:: 

    # Extract the data we want from the dataframe into X and Y arrays that can be passed to BOSS.
    # Recall that BOSS will try to minimize the output variable by default, hence to 
    # maximize yield we need to insert a minus sign in front of the y-data.
    X = df.loc[:, inputs].to_numpy()
    Y = -df.loc[:, output].to_numpy()
    
    def f_dummy(X):
        """BOSS requires an objective function coded in python to work,
        but since our objective is a physical experiment we just leave
        the function empty here and raise an error if it is called.
        """
        raise NotImplementedError
    
    # Below are some fairly typical settings for when BOSS is used for experiment design.
    # Note that you need to set the noise parameter, which should be an estimate
    # of the variance (not the standard deviation!) in the measurement of the output variable.
    bo = BOMain(
        f_dummy,
        bounds=bounds,
        iterpts=0,
        noise=0.01,  # Noise appropriate for the lignin data
        kernel="rbf",
        acqfn_name="lcb",
    )
    
    # Run one iteration with BOSS and get the results.
    res = bo.run(X, Y)
    
    # Extract the suggested new input variables, also known as the acquisition
    X_acq = res.get_next_acq(-1)
    
    # Update our dataframe and save to disk
    df.loc[len(df)] = np.append(X_acq.flatten(), np.nan)
    df.to_csv('lignin_data.csv')
    
    # Check that acquisition was successfully appended
    print(df.tail(1))

The last row of the dataframe (and csv file) now contains the new
acquisition, to continue the BO process we must measure the output
variable, i.e. yield, and update the last row of our csv file with the
measured value. We can rerun step 2 of this tutorial to get another
acquisition and so on until convergence.
