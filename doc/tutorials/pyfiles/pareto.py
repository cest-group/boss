# %% [markdown]
# # Pareto front analysis
# In this tutorial we show how to use the Pareto front post-processing feature
# of BOSS, which calculates the predicted Pareto fronts for an arbitrary number of
# BOSS models.

import numpy as np

# %%
from boss.bo.bo_main import BOMain
from boss.pp.pf_main import PFMain

# %% [markdown]
# Define the user functions within some bounds

# %%
bounds = np.array([[0.0, 3.0], [-2, 0]])


def f0(X):
    x1 = X[:, 0][0]
    x2 = X[:, 1][0]
    func = x1**4 + x2**4 + x1 * x2 - (x1 * x2) ** 2
    return -1 * func


def f1(X):
    x1 = X[:, 0][0]
    x2 = X[:, 1][0]
    func = x1**4 + x2**4 + x1 * x2 - (x1 * x2) ** 2 - 10 * x1**2
    return -1 * func


# %% [markdown]
# Run a multi-task boss run for the 2 objectives. Note that this is a
# correlated model and that default acquisition function used here is not necessarily suitable for real
# multi-objective optimizaton tasks.

# %%
bo_MO = BOMain(
    [f0, f1],  # list all tasks
    model_name='multi_task',
    bounds=bounds,
    num_tasks=2,
    kernel="rbf",
    iterpts=50,
)
bo_MO.run()

# %% [markdown]
# We can now calculate the pareto front with mesh size 30 using the models obtained from the BOSS run.
# Then we use the files generated from the Pareto front calculation to plot the pareto optimal solutions (POS)
# and the pareto front (PF):

# %%
pf = PFMain(n_tasks=2, bounds=bounds, mesh_size=30, models=bo_MO)
pf.run()

pf.plot_pos(r"x$_1$", r"x$_2$", model1=r"f$_0$", model2=r"f$_1$")
pf.plot_pf(r"f$_0$", r"f$_1$")
