#!/usr/bin/env python3
import argparse
import glob
from pathlib import Path
from typing import Literal

import jupytext
import nbformat as nbf
from nbconvert import RSTExporter


def convert_jupytext(
    input_file: str | Path,
    output_file: str | Path | None = None,
    from_fmt: Literal["py", "ipynb"] | None = None,
    to_fmt: Literal["py", "ipynb"] | None = None,
) -> None:
    input_file = Path(input_file)
    fmts = {"ipynb", "py"}
    from_fmt = from_fmt or input_file.suffix[1:]
    to_fmt = to_fmt or (fmts - {from_fmt}).pop()
    if to_fmt.startswith("py"):
        to_ext = ".py"
    else:
        to_ext = f".{to_fmt}"
    name = input_file.stem
    output_file = output_file or name + to_ext
    output_file = Path(output_file)

    with open(input_file, "r", encoding="utf-8") as fp:
        content = jupytext.reads(fp.read(), fmt=from_fmt)
    new_content = jupytext.writes(content, fmt=to_fmt)
    with open(output_file, "w", encoding="utf-8") as fp:
        fp.write(new_content)


def convert_nb_to_rst(
    input_file: str | Path, output_file: str | Path | None = None
) -> None:
    input_file = Path(input_file)
    name = input_file.stem
    with open(input_file, "r", encoding="utf-8") as nb_file:
        nb_content = nbf.read(nb_file, as_version=4)

    rst_exporter = RSTExporter()
    rst_content, _ = rst_exporter.from_notebook_node(nb_content)
    output_file = output_file or name + ".rst"
    output_file = Path(output_file)
    with open(output_file, "w", encoding="utf-8") as fp:
        fp.write(rst_content)


def expand_glob_paths(patterns: list[str]) -> list[Path]:
    """Expands any glob patterns in a list of files"""
    expanded_files = []
    for pattern in patterns:
        if any(char in pattern for char in "*?[]"):
            expanded = glob.glob(pattern)
            expanded_files.extend([Path(ex) for ex in expanded])
        else:
            expanded_files.append(Path(pattern))
    return expanded_files


def parse_cli_args() -> list[Path]:
    parser = argparse.ArgumentParser(
        description="Convert tutorial from py(ipynb) to ipynb(py) and rst"
    )
    parser.add_argument(
        "paths", metavar="P", type=str, nargs="+", help="File paths or glob expressions"
    )
    args = parser.parse_args()
    files = expand_glob_paths(args.paths)
    return files


if __name__ == "__main__":
    folders = {"ipynb": "notebooks", "rst": "", "py": "pyfiles"}

    input_files = parse_cli_args()

    fmts = {"ipynb", "py"}

    for input_file in input_files:
        from_fmt = input_file.suffix[1:]
        name = input_file.stem

        # convert py <-> ipynb
        to_fmt = (fmts - {from_fmt}).pop()
        output_file = name + f'.{to_fmt}'
        convert_jupytext(input_file, output_file)

        # convert ipynb -> rst
        input_file = name + '.ipynb'
        output_file = name + f'_py.rst'
        convert_nb_to_rst(input_file, output_file)
