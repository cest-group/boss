Cost functions
==============
Note: you can also download this tutorial as a :download:`python script <pyfiles/costs2d.py>` or a :download:`notebook <notebooks/costs2d.ipynb>`.

In this notebook we demonstrate how to apply cost functions to an acquisition function at higher dimensions. We define a
cost function as an arbitrary function that is used to modify the acquisition function. Currently, 
BOSS allows for additive or divisive (i.e. dividing) cost functions. We will use 
a 2D example.

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def f_2d(X):
        x1 = X[:,0][0]
        x2 = X[:,1][0]
        return np.sin(x1) * np.sin(x2) + 1.5 * np.exp(-((x1- 4.3)**2 + (x2 - 4.3)**2))

Similarly to the user function, the cost must be defined as a Python function. In addition,
it must be vectorized and return it's own gradient. For such an input array, a corresponding (n, 1)-shaped output array
containing the cost values must be calculated and returned together with the gradient (which will have the same shape as x).
To penalize certain regions of the solution space, we'll introduce a large cost function. This will be applied to:
1. Values of x1 greater than 5.
2. Values of x1 between 2 and 3.
3. For even stronger penalization, we'll apply a larger cost function to values of x2 greater than 6.

.. code-block:: python

    def cost(X):
        x1 = X[:,0]
        x2 = X[:,1]
        grad = np.zeros(X.shape)
        cost =  np.zeros((X.shape[0], 1))
        cost[ (x1 > 5) | ((x1 >= 2) & (x1 <= 3))] = 10
        cost[ (x2 > 6) ] = 20
        return cost, grad

We then pass the cost to BOSS using the ``costfn`` keyword and specify that we would like the cost
to be added to the acquisition via the ``costtype``` keyword. 

.. code-block:: python

    bo = BOMain(
        f_2d,
        costfn=cost,
        costtype='add', # the other option is divide
        bounds=[[0,7], [0,7]],
        seed=25,
        initpts=2,
        iterpts=30,
    )
    res = bo.run()

The effect of the cost function can be verified by running postprocessing and checking the model and acquisition function
graphs generated under ``postprocessing/graphs_models`` and ``postprocessing/graphs_acqfns``.

.. code-block:: python

    pp = PPMain(res, pp_acq_funcs=True, pp_models=True)
    pp.run()

Note that the applications of cost functions is much broader than just preventing acquisitions in certain regions.
A moderate to low cost can, e.g., be use to decrease acquisitions in a region where evaluating the objective function is extra expensive.
