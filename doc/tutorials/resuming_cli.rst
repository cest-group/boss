.. _resuming_cli:

.. NOTE: THIS TEXT IS ALREADY COMBINED TO "RESTART_CLI", CAN BE
   DELETED

.. Resuming
.. ========

.. Should the user not be satisfied with the level of BO convergence
   after the simulation is complete, it is possible to resume the
   simulation in a manner identical to :ref:`restart
   <restarting_cli>`.  In this case, the user would modify
   **boss.rst** to increment *initpts* and *iterpts* values, e.g.
   **boss_mod.rst**.  ::

	> boss op boss_mod.rst

.. For example, if we had acquired 20 points and want 30 more, we
   should set *initpts* to 20 and *iterpts* to 30. The next run will
   load all previously acquired data and continue acquiring new data
   until the new *iterpts* target is achieved. The resulting
   **boss.rst** will contain a full list of data for the two runs.

.. **Reusing a dataset.** It is possible to re-use an old dataset (or
   fit a surrogate model to any data) by placing the data list under
   the keyword *RESULTS* in the BOSS input file. Keyword *initpts*
   should be set to the number of data lines to be read under the
   *RESULTS* keyword. See :download:`Tutorial 3
   <../tutorials/cli/tutorial_3.zip>` for this use case. Should the
   list of data contain only X (states) and no Y (label), BOSS will
   iterate through the list to evaluate Y and then fit the model.

