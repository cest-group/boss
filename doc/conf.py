#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
import datetime
import os
import re
import sys

import sphinx_rtd_theme


def parse_metadata(
    file_path, extra_keywords=None, add_dunder=True, find_author_list=False
):
    """Parse core Python package metadata stored in a file."""

    metadata = {}
    if extra_keywords:
        metadata.update({k: None for k in extra_keywords})
    metadata.update(
        {
            "version": None,
            "url": None,
            "author": None,
            "author_email": None,
            "maintainer": None,
            "maintainer_email": None,
            "license": None,
            "description": None,
        }
    )

    with open(file_path) as fd:
        text = fd.read()

    # The following regex will match the contents of any non-multiline string.
    # Note: Surrounding quotes are NOT included.
    re_str_tmpl = "{}\s*=\s*['\"](.*)['\"]"

    for key in metadata.keys():
        if add_dunder:
            var_name = f"__{key}__"
        else:
            var_name = key
        match = re.search(re_str_tmpl.format(var_name), text)
        if match:
            metadata[key] = match.group(1)

    # Extra: If no author was found, look for a python list named authors or _authors__
    # and use it to build the author string.
    if find_author_list and not metadata["author"]:
        match = re.search("__authors__\s*=\s*[\[\(](.*)[\)\]]", text, re.DOTALL)
        if match:
            stringified_list = match.group(1)
            author_list = stringified_list.split(",")
            author_list = [auth.strip()[1:-1] for auth in author_list]
            metadata["author"] = ", ".join(author_list)

    metadata = {k: v for k, v in metadata.items() if v is not None}
    return metadata


sys.path.insert(0, os.path.abspath("../boss"))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.viewcode",
    # "sphinx.ext.napoleon",
    # "sphinx.ext.autodoc",
    # "sphinx.ext.autosummary",
    "numpydoc",
    "sphinx_copybutton",
    "autoapi.extension",
]
autoapi_dirs = ['../boss']
suppress_warnings = ['autosectionlabel.*']

templates_path = ["_templates"]

# The suffix(es) of source filenames.
source_suffix = [".rst"]

# The master toctree document.
master_doc = "index"

# -- Metadata ---------------------------------------------
metadata = parse_metadata("../boss/__init__.py")

version = ""
if len(version) == 0:
    version = metadata["version"]

release = version
project = "BOSS"
author = metadata["maintainer"]
copyright = f"{datetime.datetime.now().year}, {author}"

# -- Other ---------------------------------------------
language = "English"
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
pygments_style = "sphinx"

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False

# -- Options for HTML output ----------------------------------------------
# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
    "canonical_url": "",
    "analytics_id": "UA-XXXXXXX-1",  #  Provided by Google in your dashboard
    "logo_only": False,
    "display_version": True,
    "prev_next_buttons_location": "bottom",
    "style_external_links": False,
    "style_nav_header_background": "#008090",  # d9534f
    # Toc options
    "collapse_navigation": True,
    "sticky_navigation": True,
    "navigation_depth": 4,
    "includehidden": True,
    "titles_only": False,
}

html_static_path = ["_static"]

html_show_sphinx = False
html_show_copyright = True


html_sidebars = {
    "**": [
        "relations.html",  # needs 'show_related': True theme option to display
        "searchbox.html",
    ]
}


# -- Options for HTMLHelp output ------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = "BOSSdoc"


# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',
    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',
    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
# latex_documents = [
#     (
#         master_doc,
#         "BOSS.tex",
#         "BOSS Documentation",
#         "Milica Todorović, Ville Parkkinen, Henri Paulamäki",
#         "manual",
#     ),
# ]


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [(master_doc, "boss", "BOSS Documentation", [author], 1)]


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (
        master_doc,
        "BOSS",
        "BOSS Documentation",
        author,
        "BOSS",
        "One line description of project.",
        "Miscellaneous",
    ),
]

rst_prolog = open("global.rst", "r").read()
exclude_patterns = ["_build", "global.rst"]
