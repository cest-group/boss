from unittest.mock import Mock

import numpy as np
import pytest
from numpy.testing import assert_allclose

from boss.bo.acq.cost import CostAwareAcquisition
from boss.bo.acq.elcb import ELCB
from boss.bo.acq.explore import Explore
from boss.bo.acq.manager import Sequential, _is_explore


@pytest.fixture
def acqfn1d():
    model = Mock()
    mean = np.array([[1.0]])
    variance = np.array([[0.26]])
    model.dim = 2
    model.predict.return_value = (mean, variance)

    acqfn = Mock()
    acqfn.model = model
    acqfn.minimize.return_value = np.array([-1.5])
    return acqfn


def test_sequential(acqfn1d):
    bounds = np.array([[-3.0, 3.0]])
    acqman = Sequential(acqfn1d, bounds, optimtype="score", acqtol=0.5)
    assert isinstance(acqman.explorefn, Explore)

    # tesst is_loc_overconfident
    x_next = np.atleast_2d(acqfn1d.minimize(bounds))
    assert not acqman.is_loc_overconfident(x_next)
    acqman.acqtol = 0.52
    assert acqman.is_loc_overconfident(x_next)
    acqman.acqtol = None
    assert not acqman.is_loc_overconfident(x_next)

    # test acquire
    acqman.acqtol = 0.1
    assert_allclose(acqman.acquire(), x_next)


def test_is_explore():
    acqfn = Mock()
    parent = Mock()

    acqfn.__class__ = Explore
    assert _is_explore(acqfn)

    acqfn.__class__ = ELCB
    assert not _is_explore(acqfn)

    acqfn.__class__ = CostAwareAcquisition
    parent.__class__ = Explore
    acqfn.acqfn = parent
    assert _is_explore(acqfn)

    parent.__class__ = ELCB
    assert not _is_explore(acqfn)
