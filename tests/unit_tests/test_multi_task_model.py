import unittest

import numpy as np
from numpy.testing import assert_allclose, assert_array_equal, assert_equal

import boss.bo.factory as factory
from boss.bo.models.multi_task import MultiTaskModel
from boss.settings import Settings


class ModelMTTest(unittest.TestCase):
    """
    Test cases for ModelMT class
    """

    def setUp(self):
        """
        Initialize instance of ModelMT class for each test
        """
        keywords = {"bounds": np.array([[0, 1]])}
        keywords["model_name"] = "multi_task"
        settings = Settings(keywords)
        # this param array includes everything except the two noise values
        self.params = np.array([1, 0.2, -1.3, -1.5, 0.2, 0.4])
        settings["kernel"] = ["rbf"]
        settings["thetainit"] = self.params[:2]
        settings["num_tasks"] = 2
        settings["W_rank"] = 1
        settings["W_init"] = self.params[2:4]
        settings["kappa_init"] = self.params[4:]
        kernel = factory.get_kernel(settings)

        self.X0 = np.array([0.1, 0.5, 0.7])[:, None]
        self.X1 = np.array([0.1, 0.5, 0.7])[:, None]
        self.Y0 = np.array([-0.676, -0.524, -0.285])[:, None]
        self.Y1 = np.array([-0.515, -0.573, -0.398])[:, None]
        X = np.concatenate((self.X0, self.X1))
        Y = np.concatenate((self.Y0, self.Y1))
        inds = np.array([0, 0, 0, 1, 1, 1])[:, None]
        X = np.hstack((X, inds))
        noise = 1e-10
        ynorm = True

        self.model = MultiTaskModel(
            kernel,
            X=X,
            Y=Y,
            noise=noise,
            ynorm=ynorm,
        )

    def test_extend_dims(self):
        x = np.array([0.1, 0.2, 0.3])[:, None]
        inds = np.array([1, 1, 0])[:, None]

        extended = self.model.extend_input(x, inds)
        assert_equal(extended[:, -1], np.squeeze(inds))

    def test_predict(self):
        xnew = [[0.2]]
        xnew_0 = [[0.2, 0]]
        xnew_1 = [[0.2, 1]]
        pred_0 = np.array([[[-0.6873]], [[0.0475]]])
        pred_1 = np.array([[[-0.5495]], [[0.0133]]])

        m, v = self.model.predict(xnew_0)
        assert_allclose([m, v], pred_0, atol=0.001)

        m, v = self.model.predict(xnew_1)
        assert_allclose([m, v], pred_1, atol=0.001)

        m, v = self.model.predict(xnew, index=0)
        assert_allclose([m, v], pred_0, atol=0.001)

        m, v = self.model.predict(xnew, index=1)
        assert_allclose([m, v], pred_1, atol=0.001)

    def test_predict_mean_sd_grads(self):
        xnew = [[0.9]]
        xnew_0 = [[0.9, 0]]
        xnew_1 = [[0.9, 1]]
        pred_0 = np.array([[[-0.3165]], [[0.3968]]])
        grad_0 = np.array([[[-0.7459, 0]], [[1.4719, 0]]])
        pred_1 = np.array([[[-0.3857]], [[0.2103]]])
        grad_1 = np.array([[[-0.4008, 0]], [[0.7801, 0]]])

        m, s, dmdx, dsdx = self.model.predict_mean_sd_grads(xnew_0, norm=False)
        assert_allclose([m, s], pred_0, atol=0.001)
        assert_allclose([dmdx, dsdx], grad_0, atol=0.001)

        m, s, dmdx, dsdx = self.model.predict_mean_sd_grads(xnew_1, norm=False)
        assert_allclose([m, s], pred_1, atol=0.001)
        assert_allclose([dmdx, dsdx], grad_1, atol=0.001)

        m, s, dmdx, dsdx = self.model.predict_mean_sd_grads(xnew, index=0, norm=False)
        assert_allclose([m, s], pred_0, atol=0.001)
        assert_allclose([dmdx, dsdx], grad_0, atol=0.001)

        m, s, dmdx, dsdx = self.model.predict_mean_sd_grads(xnew, index=1, norm=False)
        assert_allclose([m, s], pred_1, atol=0.001)
        assert_allclose([dmdx, dsdx], grad_1, atol=0.001)

    def test_get_all_params(self):
        params = self.model.get_all_params(include_fixed=True)
        param_names = np.array(list(params.keys()))
        param_vals = np.hstack(list(params.values()))
        expected_names = [
            "kernel.variance",
            "kernel.lengthscale",
            "B.W",
            "B.kappa",
            "noise_0.variance",
            "noise_1.variance",
        ]
        assert_array_equal(param_vals[:6], self.params)
        assert_equal(param_names, expected_names)

    def test_predict_task_covariance(self):
        xnew = [[0.2, 0]]
        pred = [0.0475, 0.0219, 0.0219, 0.0133]

        cov = self.model.predict_task_covariance(xnew).flatten()
        assert_allclose(cov, pred, atol=0.001)

    def test_add_data(self):
        xnew = np.array([[0.2, 0]])
        ynew = np.array([[-1.213]])
        meannew = (np.sum(self.Y0) + ynew) / (len(self.Y0) + 1)

        self.model.add_data(xnew, ynew)
        assert_allclose(self.model.Y[-1], np.squeeze(ynew))
        assert_allclose(self.model.normmean[0], meannew)
        assert_allclose(self.model.normmean[1], np.mean(self.Y1))

    def test_add_data_batch(self):
        xnew = np.array([[0.2, 0], [0.4, 0]])
        ynew = np.array([[-1.213], [-0.003]])
        meannew = (np.sum(self.Y0) + np.sum(ynew)) / (len(self.Y0) + 2)

        self.model.add_data(xnew, ynew)
        assert_allclose(self.model.Y[-2:], ynew)
        assert_allclose(self.model.normmean[0], meannew)
        assert_allclose(self.model.normmean[1], np.mean(self.Y1))

    def test_add_data_batch_mixed(self):
        xnew = np.array([[0.2, 0], [0.4, 1]])
        ynew = np.array([[-1.213], [-0.003]])
        meannew_0 = (np.sum(self.Y0) + ynew[0]) / (len(self.Y0) + 1)
        meannew_1 = (np.sum(self.Y1) + ynew[1]) / (len(self.Y1) + 1)

        self.model.add_data(xnew, ynew)
        assert_allclose(self.model.Y[-2:], ynew)
        assert_allclose(self.model.normmean[0], meannew_0)
        assert_allclose(self.model.normmean[1], meannew_1)

    def test_redefine_data(self):
        xnew_0 = np.array([0.2, 0.4])[:, None]
        xnew_1 = np.array([0.2, 0.4])[:, None]
        ynew_0 = np.array([-1.213, -0.003])[:, None]
        ynew_1 = np.array([-0.999, -0.003])[:, None]

        xnew = np.concatenate((xnew_0, xnew_1))
        ynew = np.concatenate((ynew_0, ynew_1))
        inds = np.array([0, 0, 1, 1])[:, None]
        xnew = np.hstack((xnew, inds))
        self.model.redefine_data(xnew, ynew)

        assert_equal(self.model.get_X(index=0), xnew_0)
        assert_equal(self.model.get_X(index=1), xnew_1)
        assert_allclose(self.model.get_Y(index=0), ynew_0)
        assert_allclose(self.model.get_Y(index=1), ynew_1)
        assert_allclose(self.model.normmean[0], np.mean(ynew_0))
        assert_allclose(self.model.normmean[1], np.mean(ynew_1))

    def test_get_best_xy(self):
        x, y = self.model.get_best_xy(index=0)
        assert_equal(x, 0.1)
        assert_equal(y, -0.676)

        x, y = self.model.get_best_xy()
        assert_equal(x[0][0], 0.1)
        assert_equal(y[0], -0.676)

    def test_get_X(self):
        assert_equal(self.model.get_X(index=0), self.X0)
        assert_equal(self.model.get_X(index=1), self.X1)
        all_evidence = np.vstack((self.X0, self.X1))
        assert_equal(self.model.get_X()[:, :-1], all_evidence)

    def test_get_Y(self):
        assert_allclose(self.model.get_Y(index=0), self.Y0)
        assert_allclose(self.model.get_Y(index=1), self.Y1)
        all_evidence = np.vstack((self.Y0, self.Y1))
        assert_allclose(self.model.get_Y(), all_evidence)


if __name__ == "__main__":
    unittest.main()
