from unittest.mock import MagicMock

import numpy as np
import pytest
from numpy.testing import assert_allclose

import boss.bo.factory as factory
from boss.bo.acq.manager import Sequential


def test_select_acq_manager():
    data = {
        "batchtype": "Sequential",
        "bounds": np.atleast_2d([0.0, 1.0]),
        "optimtype": "score",
        "acqtol": None,
    }
    acqfn = None
    settings = MagicMock()
    settings.__getitem__.side_effect = data.__getitem__
    settings.is_multi = False
    acq_manager = factory.get_acq_manager(settings, acqfn)
    assert isinstance(acq_manager, Sequential)
    assert_allclose(settings["bounds"], acq_manager.bounds)

    settings.is_multi = True
    acq_manager = factory.get_acq_manager(settings, acqfn)
    desired_bounds = np.vstack((settings["bounds"], [[0, 0]]))
    assert_allclose(acq_manager.bounds, desired_bounds)


@pytest.fixture
def settings_conv():
    sts = {
        "convtype": "glmin_val",
        "conv_reltol": 1e-3,
        "conv_abstol": 1e-9,
        "conv_patience": 3,
    }
    return sts


def test_select_conv_checker(settings_conv):
    settings = settings_conv
    checker = factory.get_conv_checker(settings)
    assert type(checker).__name__ == "ConvCheckerVal"
    assert checker.rel_tol == pytest.approx(settings["conv_reltol"])
    assert checker.abs_tol == pytest.approx(settings["conv_abstol"])
    assert checker.n_iters == pytest.approx(settings["conv_patience"])

    settings["convtype"] = "glmin_loc"
    checker = factory.get_conv_checker(settings)
    assert type(checker).__name__ == "ConvCheckerLoc"
